import resultObject, { stateConstant } from "../utils/resultObject.js";
import { verifyAndAccessTokenGateway } from "../utils/serverToken.js";

export const authFields = {
  reqField: "authData",
};

const auth = {
  // Check if auth user is set
  authorizated: (req, res, next) => {
    if (res.statusCode === 200) {
      const { auth } = req[authFields.reqField];
      const data = resultObject(auth);
      res.status(200).json(data);
    }
    next();
  },
  // Return or create token
  verifyAndAccessTokenGateway: async (req, res, next) => {
    const auth = await verifyAndAccessTokenGateway(req);

    // Handle refresh only
    if (auth.expired) {
      const data = resultObject({ ...auth, auth: "" }, stateConstant.ERROR_TOKEN_EXPIRED, "Invalid token");
      res.status(401).json(data);
      next();
      return;
    }
    // Handle invalid token
    if (!auth.auth) {
      const data = resultObject(false, stateConstant.ERROR_TOKEN_INVALID);
      res.status(401).json(data);
      next();
      return;
    }

    req[authFields.reqField] = {
      auth,
    };
    next();
  },
};
export default auth;
