import reqUtil from "../utils/reqUtil.js";

const serverLog = {
  request: (req, res, next) => {
    reqUtil.req = req; // Set for read-only

    const data = {
      datetime: req._startTime,
      from: req.ip,
      method: req.method,
      path: `${req.protocol}://${req.hostname}${req.originalUrl}`,
      data: {
        path: req.route,
        header: req.headers,
        params: req.params,
        body: req.body,
      },
    };
    const message = `${data.datetime} [${data.method}] ${data.path} from ${data.from} handles(${data.data.path.stack.map(v => v.name).join(",")})`;
    console.log(`Server logging: ${message}`);
    console.log(`request  > ${data.datetime} ${JSON.stringify(data.data)}`);

    // Layer to parse object status code
    res.legacySend = res.send;
    res.send = obj => {
      req.output = obj;
      if (obj?.message?.code) res.status(obj.message.code);
      if (obj?.error?.code) res.status(obj.error.code);
      res.legacySend(obj);
    };
    res.legacyJson = res.json;
    res.json = obj => {
      req.output = obj;
      if (obj?.message?.code) res.status(obj.message.code);
      if (obj?.error?.code) res.status(obj.error.code);
      res.legacyJson(obj);
    };

    next();
  },
  response: (req, res, next) => {
    if (process.env.NODE_ENV !== "localhost") {
      const { output } = req;
      console.log(`response > ${new Date()} [${res.statusCode}] ${output ? JSON.stringify(output) : "No data"}`);
    }
  },
};
export default serverLog;
