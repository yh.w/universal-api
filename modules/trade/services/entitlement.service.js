import assetService from "./asset.service.js";
import entitlementModel from "../models/entitlement.model.js";
import { assetTypeDefault, assetSettings } from "../models/asset.model.js";

const service = {
  init: (req) => {
    entitlementModel.init(req);
  },
  commit: async (transaction, options = {}) => {
    const { items, source: { package: packageName } } = transaction;
    const assets = await assetService.getAssets(packageName);
    // Loop assets and get roles, to async entitlements
    (items || []).map(async v => {
      const { type, sku, isExternal } = v;
      const item = { type, sku, isExternal };
      // Map type with sku, if any
      const asset = await assetService.getSkuAsset(v, assets) || assets[assetTypeDefault];
      service.saveEntitlementFromTransaction({ ...item, asset }, transaction);
    });
  },
  saveEntitlementFromTransaction: async (params = {}, transaction) => {
    // Get asset settings
    const { asset } = params;

    // Build data
    const data = {
      ownerForeignKey: transaction.ownerForeignKey,
      ownerId: transaction.ownerId,
      originalTransactionId: transaction.originalTransactionId,
      transactionId: transaction.id,
      type: asset.type,
      sku: params.sku,
      meta: transaction.meta,
    };
    Object.keys(assetSettings).map(field => data[field] = asset[field]); // Pack settings value into data

    // Settings conditions handles
    if (asset.availablePeriod) {
      data.startDatetime = transaction.transactionDatetime;
      data.endDatetime = new Date(transaction.transactionDatetime.getTime() + asset.availablePeriod);
    }
    if (asset.isSnapped) data.snappedAsset = asset;

    // Save entitlement
    const result = await entitlementModel.upsert(data, { update: false });
    return result;
  },
};

export const callback = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
