import entitlementService from "./entitlement.service.js";
import assetService from "./asset.service.js";
import saleService from "./sale.service.js";
import transactionModel, { version, transactionActions, ownerForeignKey } from "../models/transaction.model.js";
import resultObject, { stateConstant, mapState } from "../../../utils/resultObject.js";

const packTransaction = async input => {
  const result = {
    valid: true,
    message: "",
  };

  // Check version
  if (input.version > version) {
    return resultObject(false, stateConstant.ERROR_DATA_INVALID, `Invalid version: >${version}`);
  }

  // Validate data
  /* Schema fields
   * {
   *   platform
   *   package
   *   country
   *   owner
   *   items
   *   currency
   *   action
   *   originalTransactionId
   *   purchaseDatetime
   *   version
   * }
   */
  let data = {};
  [
    { key: "platformTransaction", nested: "source" },
    { key: "platform", default: "web", nested: "source" },
    { key: "package", nested: "source" },
    { key: "countryCode", nested: "source" },
    { key: "owner", required: true, },
    { key: "items", required: true, },
    { key: "amount", default: "0", },
    { key: "overrideAmount", default: "", },
    { key: "currency", required: true, },
    { key: "action", required: true, validate: v => transactionActions.indexOf(v) >= 0 },
    { key: "originalTransactionId", default: null, },
    { key: "purchaseDatetime", default: new Date(), },
    { key: "version", default: version },
  ].map(v => {
    if (
      (
        (v.required && input[v.key]) // required needed
        || !v.required // required not needed
      )
    ) {
      if (
        (v.validate && typeof v.validate === "function" && v.validate(input[v.key])) // validate needed
        || !v.validate // validate not needed
      ) {
        // Add to data
        const row = input[v.key] || v.default || null;
        if (v.nested) {
          if (!data[v.nested]) data[v.nested] = {};
          data[v.nested][v.key] = row;
        } else data[v.key] = row;
      } else {
        result.valid = false;
        result.message = `Invalid field: ${v.key}`;
      }
    } else if (v.required && !input[v.key]) {
      result.valid = false;
      result.message = `Invalid field: ${v.key}`;
    }
  });
  if (!result.valid) {
    return resultObject(false, stateConstant.ERROR_DATA_INVALID, result.message);
  }

  // Validate items and calculate
  data.amount = (
    input.overrideAmount
      ? Number(input.overrideAmount) // Cast as number
      : (data.items.map(v => {
        const total = v.price * v.quantity;
        if (isNaN(total)) {
          result.valid = false;
        }
        return total;
      }).reduce((accumulator, a) => accumulator + a, 0))
  ).toString(); // Set as string
  if (!result.valid || isNaN(data.amount)) {
    result.message = "Invalid product items";
    return resultObject(false, stateConstant.ERROR_DATA_INVALID, result.message);
  }

  // Pack transaction model
  data.data = input;
  data.ownerForeignKey = ownerForeignKey;
  data.ownerId = data.owner;
  data.transactionDatetime = new Date();

  // Override `items` on `platformTransaction`
  data = await saleService.parseExternalTransaction(data); // Verify platformTransaction

  return data;
};

// Commit a transaction
// 1. only insert transaction
// 2. no update directly
// 3. use different action to set entitlements
const commit = async (input, options = {}) => {
  // Pack transaction
  const data = await service.packTransaction(input);
  if (data.status && data.status === "error") return data;

  // Validate originalTransactionId
  let { originalTransactionId } = data;
  if (originalTransactionId) {
    const originalTransaction = await service.get({
      field: "id",
      condition: "==",
      value: originalTransactionId,
    });
    if (!originalTransaction || !originalTransaction.data || originalTransaction.data.length === 0) {
      return resultObject(false, stateConstant.ERROR_DATA_INVALID, `Invalid original transaction: ${originalTransactionId}`);
    }
  }

  // Insert transaction
  const result = await service.upsert(data);
  if (result.status !== "success" || !result.data?.id) return resultObject(false, stateConstant.ERROR_DATA_INVALID, `Failed posting: ${result.message}`);

  const transactionId = result.data.id;
  if (!originalTransactionId) originalTransactionId = transactionId;

  // Save entitlements
  entitlementService.commit({
    ...result.data,
    meta: input.meta // save meta in entitlement document
  }); // do this in-sync
  result.message = mapState(stateConstant.SUCCESS_DATA_ASYNC_ACPT);

  return result;
};

const getTransactions = async (queries, options = {}) => {
  const data = await transactionModel.get({
    where: queries,
    orderBy: queries?.orderBy ? queries.orderBy : (!queries?.orderBy ? {} : { field: queries ? queries.field : "createDatetime", sort: "asc" }),
  }, options);
  return data;
};

const upsertTransaction = async (data, options = {}) => {
  const result = await transactionModel.upsert(data, options);
  return result;
};

const service = {
  init: (req) => {
    transactionModel.init(req);
    // Init dependencies
    entitlementService.init(req);
    assetService.init(req);
    saleService.init(req);
  },
  packTransaction,
  get: getTransactions,
  upsert: upsertTransaction,
  commit,
};

export const callback = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
