import mongoose from "mongoose";
import modelUtils, { validator } from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "assets";
export const assetTypes = [
  "comsumable", // single entry
  "permanent", // non-comsumable
  "subscription", // auto renewable subscription, default isAutoRenew true
  "periodic", // nonrenewable subscription
];
export const assetTypeDefault = assetTypes[1];

export const assetSettings = {
  startDatetime: {
    type: Date,
    default: null,
  },
  endDatetime: {
    type: Date,
    default: null,
  },
  snappedAsset: {
    type: Schema.Types.Mixed,
    default: null,
  },
  // Flags for type status
  isExpired: {
    type: Boolean,
    default: false,
  },
  isAutoRenew: {
    type: Boolean,
    default: false,
  },
  isSnapped: {
    type: Boolean,
    default: true,
  },
};

export const schema = {
  type: {
    type: String,
    enum: {
      values: assetTypes,
      message: "{VALUE} is not supported",
    },
  },
  roles: [{ // asset_role
    type: String,
  }],
  name: {
    type: String,
  },
  package: {
    type: String,
    default: "general",
  },
  skus: [{ // map external sku types
    type: String,
  }],
  ...assetSettings,
  availablePeriod: { // in ms
    type: Number,
    default: null,
  },
  // For customised fields
  meta: {
    type: Schema.Types.Mixed,
    default: {},
  },
};

const events = {
  onPackData: null,
  afterRetrive: (data) => {
    ["createDatetime", "updateDatetime", "startDatetime", "endDatetime"].map(v => { if (data[v]?.toDate && typeof data[v].toDate === "function") data[v] = data[v].toDate(); }); // Parse firebase Timestamp
    return data;
  },
};

const associations = {
};

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  schema,
});
