import redeemService from "./services/redeem.service.js";
import { path } from "../moduleFrame.js";
import assetModule from "../trade/asset/index.js";
import authData from "../../utils/authData.js";

export const prefix = "/redeem";
const modulePath = prefix;

const insert = async (req, res) => {
  redeemService.init(req);
  const result = await redeemService.insert(req.body || {});
  res.send(result);
};

const packOriginData = req => {
  const user = authData(req);
  return {
    platform: user.platform,
    environment: user.environment,
    package: user.package || user.token.aud,
    owner: user.user,
    ip: req.headers.ip || req.headers["x-forwarded-for"] || "",
  };
};

const redeem = async (req, res) => {
  const code = req.params?.code ? req.params.code : "";
  const user = authData(req);

  redeemService.init(req);
  const result = await redeemService.redeem({
    code,
    origins: packOriginData(req),
  });
  res.send(result);
};

const authorizated = async (req, res) => {
  redeemService.init(req);
  const result = await redeemService.authorizated(packOriginData(req));
  res.send(result);
};

const module = {
  init: builder => {
    if (process.env.NODE_ENV === "localhost") builder("put", path("", modulePath), insert, false);
    builder("get", path("/:code", modulePath), redeem, true);
    builder("post", path("", modulePath), authorizated, true);

    assetModule.init(builder);
  },
};

export default module;
