import mongoose from "mongoose";
import modelUtils, { validator } from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "redeem_codes";

export const codeTypes = [
  "general",
  "custom_data",
];

export const schema = {
  name: { // Display only
    type: String,
  },
  code: { // String code, unique
    type: String,
    required: true,
    unique: true,
  },
  /*  Feature:
   *  Is redeemed periods
   */
  startDatetime: { // For expires
    type: Date,
    default: null,
  },
  endDatetime: { // For expires
    type: Date,
    default: null,
  },
  /*  Feature:
   *  Redemption limits
   */
  redeemLimits: { // Limits all-over
    type: Number,
    default: null,
  },
  redeemCounts: { // Count redeemed, init 0
    type: Number,
    default: 0,
  },
  ownerRedeemLimits: { // Limits per owner
    type: Number,
    default: 1,
  },
  /*  Feature:
   *  Life-time after redeemed
   */
  availablePeriod: { // Period for redeemed availability from redeemDatetime, in ms
    type: Number,
    default: null,
  },
  /*  Feature:
   *  platform blockers
   */
  platforms: [{
    type: String,
  }],
  /*  Feature:
   *  ip blockers
   */
  regionsIncluded: [{ // [] of restricted regions, by country codes iso3
    type: String,
  }],
  regionsExcluded: [{ // [] of restricted regions, by country codes iso3
    type: String,
  }],
  /*  Feature:
   *  types, code / (GET)/redeem/:code behaviors
   */
  type: {
    type: String,
    enum: {
      values: codeTypes,
      message: "{VALUE} is not supported",
    },
    default: "general",
  },
};

const events = false;

const associations = false;

const viewSuffix = false;

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  viewSuffix,
  schema,
});
