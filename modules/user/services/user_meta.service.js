import userService from "./user.service.js";
import callbacksUtils from "../../../utils/callbacksUtils.js";

const getUserMeta = async (tokenUser = {}) => {
  let theUser = tokenUser;

  theUser = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "getUserMeta", data: theUser });

  const id = theUser.user.id;
  const where = { field: "id", condition: "==" };
  where.value = id;

  const result = await userService.get(where);
  result.data = result?.data && result.data.length > 0 ? result.data[0] : {}; // Forced transform result object

  result.data = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "getUserMeta", data: result.data });

  return result;
};

const service = {
  init: req => {
    userService.init(req);
  },
  getUserMeta
};

export const callbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
