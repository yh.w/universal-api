import mongoose from "mongoose";
import modelUtils from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "user_profiles";

export const schema = {
  userId: {
    type: String,
    required: true,
  },
  displayProfileId: {
    type: String,
    unique: true
  },
  meta: {
    type: Schema.Types.Mixed
  },
};

const dataSource = "firestore";

export default modelUtils({
  dataSource,
  collection,
  schema,
});
