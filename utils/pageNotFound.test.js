import pageNotFound from "./pageNotFound.js";

describe("pageNotFound, return res object", () => {
  it("Should set response data", () => {
    const mockResponse = () => {
      const res = {};
      res.status = jest.fn().mockReturnValue(res);
      res.json = jest.fn().mockReturnValue(res);
      return res;
    };

    const req = {};
    const res = mockResponse();
    pageNotFound(req, res);

    expect(res.status).toBeCalledWith(404);
    expect(res.json).toBeCalledWith({
      result: false,
      message: "Page not Found.",
    });
  });
});
