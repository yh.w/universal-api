import mongoose from "mongoose";

const mongooseUtils = {
  model: null,

// TODO
// 1. create from mongoose package Model from Schema
// 2. build getDocuments()
// 3. build upsertDocument()

};

const mongooseUtilsInit = schema => {
  // Create Model from Schema for saving
  const Model = mongoose.model("Model", schema);
  mongooseUtils.model = Model;

  return mongooseUtils;
};

export default mongooseUtilsInit;
