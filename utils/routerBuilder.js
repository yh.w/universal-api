import serverLogging from "../middlewares/serverLogging.js";
import authorization from "../middlewares/authorization.js";

const routerBuilder = router => (method, path, callback, auth = false) => router[method](
  path,
  serverLogging.request,
  auth ? authorization.verifyAndAccessTokenGateway : (req, res, next) => { next(); }, // trigger authorization
  (req, res, next) => {
    if (!req.output) callback(req, res); // Only callback if no header set
    next();
  },
  serverLogging.response
);

export default routerBuilder;
