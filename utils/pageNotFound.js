export default (req, res) => {
  res.status(404).json({
    result: false,
    message: "Page not Found.",
  });
};
