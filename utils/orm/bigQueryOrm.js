import { BigQuery } from "@google-cloud/bigquery";
import sqlUtils from "./utils/sqlUtils.js";
import resultObject, { stateConstant } from "./utils/resultObject.js";
import database from "../../config/database.js";

// Db configs
const projectId = database.bigquery.project_id;

// Init BigQuery
const connection = new BigQuery({
  credentials: database.bigquery,
  projectId,
});

const getDataset = (datasetId, environment = "production") => connection.dataset(`${orm.db.prefix ? `${orm.db.prefix}_` : ""}${environment === "production" ? datasetId : `${datasetId}_staging`}`);
const getTable = (dataset, tableName) => dataset.table(tableName);
const initTable = async (datasetId, options = { environment: "production", prefix: "", initModels: [] }) => {
  if (!datasetId && !orm.db.datasetId) return {};
  if (datasetId) orm.db.datasetId = datasetId;
  if (options.prefix) orm.db.prefix = options.prefix;

  let dataset = getDataset(orm.db.datasetId, options.environment);
  try {
    await dataset.get();
  } catch (ex) {
    if (!ex.errors) return resultObject(false, ex.message);

    if (ex.errors[0].reason !== "notFound") {
      return resultObject(false, stateConstant.ERROR_UNHANDLED);
    }
    // Create if not exist
    [dataset] = await connection.createDataset(dataset.id);
  }

  let table;
  const initModels = options.initModels || [];
  for (const i in initModels) {
    table = getTable(dataset, initModels[i].collection);
    try {
      await table.get();
    } catch (ex) {
      if (ex.errors[0].reason !== "notFound") {
        return resultObject(false, stateConstant.ERROR_UNHANDLED);
      }
      // Create if not exist
      const options = {
        schema: parseSchemaBQ(initModels[i].schema.paths),
      };
      [table] = await dataset.createTable(table.id, options);
    }
  }

  return {
    db: dataset.id,
    conn: table, // Model table instance as query connection
  };
};
const parseSchemaBQ = (schema) => {
  delete schema._id;
  return Object.keys(schema).map(v => {
    const key = v;
    let type = schema[v].instance.toLowerCase();

    // Map type of BigQuery
    if (type === "number") type = "numeric";
    if (type === "date") type = "datetime";

    return `${key}:${type}`;
  }).join(", ");
};

const orm = {
  db: {
    prefix: "",
    datasetId: "",
  },
  initTable,
  parseSchemaBQ,
  utils: sqlUtils,
};
export default orm;
