export default {
  sqlBuilder: {
    colRow: (schema, data) => {
      const output = {
        cols: [],
        data: [],
      };
      Object.keys(schema).forEach(v => {
        if (["_id", "__v"].indexOf(v) >= 0) return;

        output.cols.push(v);
        switch (v) {
          case "status":
            return output.data.push("\"active\"");
          case "createDatetime":
          case "updateDatetime":
            return output.data.push("CURRENT_DATETIME()"); // bigquery , mysql now()
          case "createdBy":
          case "updatedBy":
            return output.data.push("\"SYSTEM\"");
          default:
            return output.data.push(schema[v].type.name === "Number" ? `${data[v] || ""}` : `'${data[v] || ""}'`);
        }
      });

      return output;
    },
  },
};
