const errors = Object.freeze({
  ERROR_UNHANDLED: {
    message: "Unhandled errors",
    type: "Unhandled",
    code: 500,
  },
  ERROR_TOKEN_INVALID: { // 401
    message: "Invalid token",
    type: "TokenInvalid",
    code: 401,
  },
  ERROR_TOKEN_EXPIRED: { // 401
    message: "Expired token",
    type: "TokenExpired",
    code: 401,
  },
  ERROR_FORBIDDEN: { // 403, valid but not permised
    message: "Forbidden",
    type: "Forbidden",
    code: 403,
  },
  ERROR_DATA_MISSED: {
    message: "Missed data",
    type: "DataMissed",
    code: 422,
  },
  ERROR_DATA_INVALID: {
    message: "Invalid data",
    type: "DataInvalid",
    code: 422,
  },
  ERROR_DATA_DUPLICATED: {
    message: "Duplicated data",
    type: "DataDuplicated",
    code: 409,
  },
  EXCEPTION_DATA_NOT_FOUND: { // 204, not found
    message: "Not found data",
    type: "DataNotFound",
    code: 204,
  },
  SUCCESS_DATA_ASYNC_ACPT: { // 202, accept for asynchronous request, check later
    message: "Accepted data",
    type: "DataAccepted",
    code: 202,
  },
  SUCCESS_DATA_CREATED: { // 201, created
    message: "Created data",
    type: "DataCreated",
    code: 201,
  },
});

export const stateConstant = Object.fromEntries(Object.entries(errors).map(([k, v]) => [k, k]));

export const mapState = (state) => {
  // Search error with state
  return errors[state] || state;
};

// Return standard object for app, view
// Ref. Google JSON Style Guide
export default (data, err, message, errOnData = false) => {
  const result = {
    status: false,
    data: null,
    message: message || null,
  };

  if (data) {
    result.status = "success";
    result.data = data;
  }
  if (err) {
    const stateErr = mapState(err) || err;
    result.status = "error";
    result.data = errOnData ? stateErr : data; // if not on data, forced empty field, data
    result.error = errOnData ? null : stateErr; // if not on data, data on error field
  }
  return result;
};
