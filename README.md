# An Universal API Framework

Utilities of API development.

## Features

- Express.js https://expressjs.com/
- RESTful
- Versioning route prefix
- Auth
  - JWT Authorization
  - Hashed password users
  - OAuth Providers
- **Modularize** features supported (see /modules)

## Environment variables

To start application successfully, copy `.env.sample` to index directory (or source directory when used as submodule)

`yarn set-env` will provide instruction to set environment variables

`.env.sample.yaml` is another environment variables sample for cloud deployment.

## Deployments

**Onto Cloud Functions**

with Google Cloud SDK `gcloud functions deploy <function_name> --project <project_id> --env-vars-file <env-vars.yaml> --build-env-vars-file <env-vars.yaml> --runtime nodejs14 --trigger-http --entry-point app`

Add `Permissions -> Principal -> allUsers : Cloud Functions Invoker` if need public access

**Accessing Firestore (for local testing)**

Navigate to APIs and Services -> Credentials

Look for web application in OAuth 2.0 Client IDs

Add the desired origins to `Authorized JavaScript origins`

## Submodule

Use the framework as a **Git Submodule**

### Init & use Git submodule

1. Running git commands

   a. `git submodule add <repository> [<path>]` to add submodule

   b. `git submodule init` to init submodule sources

   A new file `.gitmodules` will be created.
    
1. `cp sub-app.package.json <main_directory>/package.json` to copy default `package.json` before adding submodules

### Update submodule

`git submodule update --init --recursive --remote`

or

`cd <submodule_src> && git pull origin master` in case walk into submodule src directorties (in-case for some branches operations)

### Build source with framework from other apps

`<main_directory>` Main directory refer to source directory you use & run the express app, commonly under `/sub_modules`

Remarks: `app.js` being used is the default triggering entry of the express app sources.

1. Add local dependency as module packages

    `yarn add <submodule_path> --save` with path begin with `./`
  
    or

    `yarn add link:<main_directory>` using `link`

1. `cp .eslint* <main_directory>` source for eslint

1. `cp .babelrc <main_directory>` source for jest test

1. `cp .gitignore <main_directory>` for git ignore

1. `cp sub-app.js <main_directory>/app.js` main script

1. Serve with `./app.js`

1. Build modules in `/src`

#### Development notes for modules in `/src`

  1. Create routes modules at directory like `/src/routes`
  1. Add those routes at `beforeRoute()` callback with `app.use()`
  1. At each routes, refer modules from directory like `/src/modules` to consolidate sources
