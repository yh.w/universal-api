import v1Routes from "./v1.routes.js";
import pageNotFound from "../utils/pageNotFound.js";

const addRouteVersions = (app, config = { options: null, callbacks: {} }) => {
  const { options = null, callbacks = {} } = config;
  // Routes

  // Callback for external
  if (typeof callbacks.beforeRoute === "function") callbacks.beforeRoute(app);

  // v1
  app.use("/v1", v1Routes(options));

  // Undefined asset or api routes should return a 404
  app.route("/:url(api|components|app|bower_components|assets)/*").get(pageNotFound);

  // Non-match paths
  app.route("/*").get(pageNotFound);

  // Callback for external
  if (typeof callbacks.afterRoute === "function") callbacks.afterRoute(app);
};

export default addRouteVersions;
