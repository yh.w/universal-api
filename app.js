import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import helmet from "helmet";
import cors from "cors";
import databaseConfig from "./config/database.js";
import middlewareConfig from "./config/middlewares.js";
import routes from "./routes/routes.js";

// Express configs
const port = process.env.PORT || 3000;
const buildApp = (instance) => {
  instance.use(express.json());
  instance.use(bodyParser.urlencoded({ extended: true }));
  // Middlewares
  instance.use(morgan("common"));
  instance.use(helmet(middlewareConfig.helmet));
  instance.use(cors(middlewareConfig.cors));
};
const connApp = (instance, options = { externals: { packages: [], options: {} } }) => {
  // Db connections
  databaseConfig.firestore.init(instance);
  if (options.externals) databaseConfig.firebaseAdminExternal.setExternal(options.externals.packages, options.externals.options);
};
let server = null;
const listenServer = (newApp = null) => {
  server = (newApp || app).listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
  });
};

// Main app
const app = express();
buildApp(app);
routes(app);
connApp(app);
if (process.env.NODE_ENV === "localhost") { // Only serve on development
  listenServer();
}

// Default deploy endpoint, to prevent build with error when deploying to cloud, if needed
const deploy = () => {};

const subApp = (config = {
  route: {},
  app: {
    externals: { packages: [], options: {} },
  },
}) => {
  if (process.env.NODE_ENV === "localhost") { // Only serve on development
    console.log("Stop main server.");
    server.close();
    console.log("Register new routes.");
  }

  // sub app
  const newApp = express();
  buildApp(newApp);
  routes(newApp, config.route); // Register default routes with options, callbacks
  connApp(newApp, config.app);

  if (process.env.NODE_ENV === "localhost") { // Only serve on development
    console.log("Restart sub-server.");
    listenServer(newApp);
  }

  return newApp;
};

export { app, deploy, subApp };
